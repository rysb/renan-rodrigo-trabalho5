package br.unicamp.ic.extensao.inf335;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Scanner scan = new Scanner(System.in);
    	System.out.print( "Digite o numero da temporada e pressione [ENTER]: " );
    	String temporada = scan.nextLine();
    	
    	try {
    		MongoClient client = MongoClients.create("mongodb://localhost:27017");
        	
        	MongoDatabase db = client.getDatabase("friends");
        	
        	MongoCollection<Document> collection = db.getCollection("samples_friends");
        	
        	Iterable<Document> documents = collection.find(new Document("season", Integer.parseInt(temporada)));
        	for(Document d : documents) {
        		System.out.println(d.get("name"));
        	}    		
    		
    	}catch(Exception ex) 
    	{
    		System.out.println(ex.getMessage());
    	}
    	finally
    	{
    		scan.close();
    	}
    	
    	
    	
    }
}
